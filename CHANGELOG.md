# Changelog

## v2.9.0

Mainnet release

## v2.9.1

#### Changes

- PAR-2387: Report signature metrics from proof of justification (fast-track-node:2.9.1)
- PAR-4196: Remove openssl from docker image
- PAR-4197: Producers experiencing SIGSEGV (Update JRE to Temurin 17.0.3+7)

## v2.9.2

#### Changes

- PAR-4171: Introduce infrastructure payer to handle payment for tasks necessary for the
  infrastructure

#### Registered votes

- PAR-2916: Block Producer Orchestration: Invocation to get information about a registered block
  producer (Poll hash: 3c5be8e2685e1f485d2bc0fbe8affa838a63af0ffb6b5ea49c445622732c1618)

## v2.9.3

#### Changes

- PAR-3952: Service for monitoring of ETH/USD price and publish it to PBC

#### Registered votes

- Composite poll a3bca3179c21074f7ed405e0e8c44d51fb2f7ec22a4671915a53e9d465069e0b
  - PAR-4285: Allow any governance contract to interact as a small oracle on the large oracle
    contract
  - PAR-4171: Introduce infrastructure payments in account plugin
  - PAR-3963: Use collected signature metrics to filter the producers being paid in an epoch

## v3.0.0

#### Changes

- PAR-4314: Release version 3.0.0 of Partisia Blockchain

#### Registered votes

- Composite poll 6d77ca1d5f66fcfd3ade78e97165ec9abf0b57c755a934e301086c95fe1ac158
  - Deploy ZK node registry at 01a2020bb33ef9e0323c7a3210d5cb7fd492aa0d65
  - Deploy REAL preprocess at 01385fedf807390c3dedf42ba51208bc51292e2657

## v3.0.1

Enables on- and off-chain components needed to run ZK computations on Partisia Blockchain.

#### Changes

- PAR-3916: Enable off-chain REAL ZK components

#### Registered votes

- Composite poll 076d8ecb060b5866e98a8d13d282190592b40db9b1795974e74ea829e06b0d6a
  - Deploy real deploy contract at 018bc1ccbb672b87710327713c97d43204905082cb

## v3.0.2

Prepare producer configuration to allow configuring rest URL to Polygon PoS chain.

#### Changes

- PAR-4181: Add polygon URL to config

## v3.0.3

#### Changes

- PAR-3869: Code quality improvements in peer discovery
- PAR-3906: Cleaner shutdown of network connections
- PAR-3939: Improved clean up of network connections
- PAR-4375: Improved peer discovery for block producers
- PAR-4489: Additional rest endpoint for querying blocks and transactions
- PAR-4576: Performance improvements for BLS
- PAR-4717: Deterministic vote identifiers

#### Registered votes

- Create BYOC symbol (id: 84ad54fba83204f3e5f595ea932c137bc5ff4c0c8aca37918e0409ffa50b1ea8)
  - Create BYOC symbol for "POLYGON_USDC"
- Deploy contract (id: 2c1ca8006d097a741661d0619df2b2c101713d0aeaba669e1f5c83a06ae22089)
  - Deploy BYOC outgoing for Polygon USDC to 04adfe4aaacc824657e49a59bdc8f14df87aa8531a
- Deploy contract (id: 8d2317601672d63545a6ef3bdcf99181e65e6cd432ebfb08d5d7c874d1719a72)
  - Deploy BYOC incoming for Polygon USDC to 042f2f190765e27f175424783a1a272e2a983ef372
- Update account plugin (id: 6aa391a26e6456f3cfc40d95d3f3f8eb8d7cbaac03765a01f0df22ae3eef199e)
  - PAR-4085: New invocation for collecting fees including infrastructure fees
- Update large oracle contract (id:
  3a8fb70c7be7c6b9d904dd634b170bcc55c2f69bc138c252f48ee1a7375e83cc)
  - PAR-4328: Allow small oracle to lock stakes for a specific user
  - PAR-4155: Reset disputes when enabling new large oracle
  - PAR-3929: Improve code quality of tests by using invocation byte instead of enum ordinal
- Update BPO contract (id: c3d766edce1b6d68dd319e748651e6bf995363471a35bac5b473594988372ad1)
  - PAR-4597: New invocation that allows KYB providers to add a confirmed BP including all
    information
  - PAR-3221: Producers are able to resign as block producers
- Update voting contract (id: 69ca5ecbaafc31b6cf0918e1acd6df7c0b0ee9ba20b723567eb0333f594e7026)
  - PAR-3696: CHECK_UPDATE removes a poll if it has timed out

## v3.0.4

#### Registered votes

- Update fee distribution contract (id:
  3a8fb70c7be7c6b9d904dd634b170bcc55c2f69bc138c252f48ee1a7375e83cc)
  - PAR-4317: Introduce infrastructure fee pool
- Update wasm deploy contract (id: 15fa35521d6a92028745d299214a90e45edf8ba4eb5c7f4c09821c67e4be3bbb)
  - Update to latest binder to be compatible with latest SDK

## v3.0.5

#### Registered votes

- Update fee distribution contract (id:
  b5ae9ceab7f83fce2fb619ea706bb4cb087f1e4a89a6b6a459bac12b614ffa81)
  - PAR-5097: Potential bug when paying out to producers
- Update block producer orchestration (id:
  d0afd57de26a57e541faeb318bfd311af6f610ed522279aa90fe96cb18207776)
  - PAR-5110: Allow assignment of new KYC providers when upgrading the contract
  - PAR-4444: Register Synaps KYCB contract as a KYC provider
- Deploy Synaps KYCB contract (id: 2acd5005a13d801da0344942580e6266ff609839adc0d8682180db569418d99f)
  - PAR-4444: Initial support for Synaps signed KYC/B

## v3.0.6

#### Changes

- PAR-3691: Improve handling of transactions sent by nodes
- PAR-4177: Ensure reset block dto is json serializable
- PAR-4256: Improved quality of tests
- PAR-4273: Better coordination between price oracle node and contract
- PAR-5082: Record support in rest resources

#### Registered votes

- Update account plugin (id: b589b3eca5945cfcc02b0dd189330c7c176ef4510a1ba809c560ad03dc464b2f)
  - PAR-3773: Add invocations for delegated staking
- Update mpc token contract (id: dfea64e923607be5042c00ac9bd2c1cba33616b8f6004726593dd1a87196e6ff)
  - PAR-3773: Add invocations for delegated staking

## v3.0.7

#### Changes

- PAR-5090: Bug fix for always true contract existence check. Behind feature flag: '
  CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT'
- PAR-5245: Producer that are not longer CONFIRMED but still part of the active committee should be
  able to participate in peer discovery
- PAR-4263: Initial Rosetta implementation

#### Registered votes

- Update Synaps contract to 3.0.7 (id:
  73240b99f3cb91f4d1034a0f539befd18e702722552bc4a0febeb59f69e63d56)
  - PAR-5448: Only the producer himself should be allowed to register using Synaps data
- Enable feature flag CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT (id:
  45d34ad04f4be76164ab76a13eb6191ba9ad69f33fe04d67177efddd4910def2)
  - PAR-5090: Bug fix for always true contract existence check. Behind feature flag: '
    CHECK_EXISTENCE_FOR_CONTRACT_OPEN_ACCOUNT'

## v3.0.8

#### Changes

- PAR-5469: Check account existence consistently in Rosetta implementation
- PAR-5503: Initial system update check after 30 seconds and then each hour

#### Registered votes

- Update Mpc Token conatract to 3.0.15 (id:
  cac5a58d1de97ef31300696ed6b2a21ae8a52631223b6dd8f181a0609774751e)
  - PAR-5311: Bug fix for delegated staking
- Update account plugin to 3.0.19 (id:
  2b66bf8040d03332e6be5a625b3f0ab5fb9da654db4fb559d44739651f41251c)
  - PAR-5493: Compatibility change to replace ETH with MPC_ROPSTEN
  - PAR 5311: Bugfix for retrying delegated stake transaction
- Update ZK node registry to 3.0.16 (id:
  99107732603c7e8ad5fa3725280cd9067197b7bef02b57142002c5158a8e2019)
  - PAR-4820: Zk Protocol Version tracked per node in registry
  - PAR-4899: Select ZK nodes based on jurisdiction
  - PAR-5347: Change the requirement for ZK nodes to 75K MPC
- Update Real Deploy to 3.0.15 (id:
  0796bf532aebb86740c83e57a441dfc9dc1475f695ca5c9ce8aacd6a9bcf8a3c)
  - PAR-5077: Zk Protocol Version in requestNodes
  - PAR-4900: Ability to deploy ZK contracts with select ZK jurisdictions
  - PAR-5450: onUpgrade implementation

## v3.1.0

#### Changes

- PAR-5468: Cleanup metadata from Rosetta preprocess
- PAR-5421: Validate network identifier in Rosetta offline resource
- PAR-4584: BYOC-service should not depend on contract jars
- PAR-4865: Added support for record state serialization

#### Registered votes

- Update BP orchestration to 3.3.0 (id:
  1872cfe4981f7efd07beb623a428f9fe3bdcef6d44f6a01538444c1cd5164c68)
  - PAR-5767: Ensure that confirmedSinceLastCommittee is incremented when a new producer is
    registered through Synaps

## v3.2.0

#### Changes

- PAR-4335: Vote for price oracle disputes
- PAR-4724: Ensure Rosetta integration handles new BYOC transfer invocations
- PAR-3989: Include inline executed events when getting transactions for block
- PAR-5074: Zk Protocol Version automatically updated by real-node
- PAR-5413: Remove hard-coded pre-process contract hashes in real-node
- PAR-5480: Remove test dependencies on on-chain components in real-node
- PAR-5254: Improve errors in Rosetta API
- PAR-5582: Automatically match node version to pom version in Rosetta API
- PAR-3303: Improved test quality in core/server
- PAR-3905: Limit the amount of messages sent during fasttrack shutdown synchronisation

#### Registered votes

- Update wasm deploy contract to 3.2.0 (id:
  799de4068c44e40457acd746b15e89de0c58fad99aa30275aa0ee441d83a1948)
  - Update binder to 3.7.0
    - PAR-4722: Support for return values
    - PAR-5666: Support multiple binder versions
- Update large oracle to 3.4.0 (id:
  d64f5cc4cdc39a0dd6928b8e5066dd65b8a7fa97dd84724c82a939e23c8afa83)
  - PAR-5494: Replace "MPC_ROPSTEN" with "ETH" as the symbol in state
  - PAR-5563: Allow small oracles to release tokens locked to oracle
- Update account plugin to 3.4.0 (id:
  a07f28f4596cabd40d63cbebd57d8b51421976f28bff9478a970ae84919f933b)
  - PAR-4724: Invocations for transferring BYOC
  - PAR-5495: Update BYOC symbol from MPC_ROPSTEN to ETH
- Update BYOC incoming for ETH to 3.3.0 (id:
  f28802aa2270a02c6e09cc412d4b76a221405e7296803d861f206396270ba302)
  - PAR-4111: Adding invocation byte to callbacks
  - PAR-5496: Rename MPC_ROPSTEN to ETH
- Update BYOC outgoing for ETH to 3.3.0 (id:
  c2cf975496a88a62a83530b8e57dcea70ff3051e169793709a02f7d2e67ffa8b)
  - PAR-4111: Adding invocation byte to callbacks
  - PAR-5496: Rename MPC_ROPSTEN to ETH
- Update MPC token contract to 3.3.0 (id:
  57c0df697d4a63bc39783dc5b1a1411c0b7ee85166d6818169f3745397989f7c)
  - PAR-4724: Invocation for transferring BYOC

## v3.3.0

#### Registered votes

- Update real wasm deploy contract to 3.2.0 (id:
  b2cea0f01777f02d009d8af067694f2d58a9853256e10a9e4b750fa5aaf481b8)
  - Update real binder to 3.9.0
    - PAR-5449: Computation Public Inputs, Binder side
    - PAR-4722: ZK Return values, Binder side
    - PAR-5666: support multiple binder versions
- Update account plugin to 3.7.0 (id:
  1ac5b6dc59227fe1262d245606cc003c7e67121eaa19f087e196d38bf23e2191)
  - PAR-5793: Remove stakes to contract when value is 0
  - PAR-5935: Update guards for reduce and retract delegated stakes

## v3.4.0

#### Registered votes

- Update account plugin to 3.10.0 (id: 70ab3aa9e076e36c140a91a42ccf6d5ca2cae1b2ea24b43b418936de530d184b)
  - PAR-5977: Handle timestamps for epoch as unsigned 
  - PAR-5994: Check pending unstakes should also handle retractedStakes
- Update BP orchestration to 3.5.0 (id: 6dfa7979fdda3acceccb2531d85c7ab195ecdb1d654ec583ae08a1f7b2f813a0)
  - PAR-5986: When confirming a BP we should trigger a new committee if enough has changed
- Update rewards contract to 3.4.0 (id: 07eed56eb40d4369024d8911ed3f8126f9863f968e1e84df5bdfcb1b608631b9)
  - PAR-5977: Handle timestamps for epoch as unsigned

## v3.5.0

#### Changes

- PAR-5805: Move ownership of URLs to server project
- PAR 5398: Rosetta: Ensure transaction is from the specified block in /block/transaction
- PAR-5977: Handle timestamps for epoch as unsigned
- PAR-5848: Ability to transfer StateSerializable to new storage
- PAR-5993: Potential concurrency issue in ChainStateCache

#### Registered votes

- Update BP orchestration to 3.7.0 (id:
  2b83c3aaa123f005476ac7fa06ae148aafe08bf53365a54c48ef88169f5b5264)
  - PAR-6125: Migrate all 'PENDING_UPDATE' producers to 'CONFIRMED'

## v3.6.0

#### Changes

- PAR-5491: Allow specifying explicit contracts in TCP listener client
- PAR-6202: RealNode input independence
- PAR-6211: Prepare for BNB BYOC

#### Registered votes

- Create BYOC symbol for BNB (id: 6da6dca4f16c9c7d1b8edef1e3b01ea4bfceb3e3d18d96a6cd0335539eb62ad7)
- Update account plugin to 3.17.0 (id: f0dab558c21f8dbd7ebd46869331de39a0cf6c4c736f30476457da2847a4d5d0)
  - PAR-5802: Additional input validation on amounts
  - PAR-6018: Additional state validations
- Update large oracle to 3.10.0 (id: e10bcd404b5d753830ff85f196dac74e7e0d356923ee4ae264664d093b8f3d1d)
  - PAR-5943: Include oracle id when sending 'insufficient funds'
  - PAR-5970: New invocation for creating dispute poll that throws on insufficient funds
- Update public deploy to 3.2.0 (id: e331b8059e81b74df62eaf5ab23675617f290c04ff5ba4eabf5ab93ca0fb7632)
  - Fix group id

## v3.7.0

#### Changes

- PAR-6512: Use SecureRandom when generating seeds in real node 
- PAR-6517: Use SecureRandom when generating kademlia key
- PAR-6394: Rosetta: BalanceCalculator does not account for released tokens

#### Registered votes

- Deploy BYOC-incoming 3.3.0 for BNB at 047e1c96cd53943d1e0712c48d022fb461140e6b9f (id: 82273b49e27e2be5c8255060f86fdf1eb9bec0a04ea923cdb3bd17eca3591a77)
- Deploy BYOC-outgoing 3.6.0 for BNB at 044bd689e5fe2995d679e946a2046f69f022be7c10 (id: cbc764d163dea88b376b6a8041c99b7e1613491f4262318faa42dfde1639b4c2)
- Update account plugin to 3.21.0 (id: 86be2a185721df5af03ab28565f46bec556e5b92d7340ec71adf84f57b6f6222)
  - PAR-5917: Code cleanup of invocations
  - PAR-5917: Allow contracts to hold BYOC
  - PAR-6029: Improved javadoc
- Update BP orchestration to 3.9.0 (id: cae11305158953c708b93ecfa593fec063cd1532a6cc68306ed44b6e9d8ce9ce)
  - PAR-3387: Bitmap allocates unnecessary byte
  - PAR-5927: Use threshold based on active producers when selecting key

## v3.8.0

#### Changes

 - PAR-6613: Update parent to include latest external libraries

## v3.9.0

#### Changes

- PAR-6051: Avoid sending multiple transaction for same attestation 
- PAR-6683: Update external dependencies

#### Registered votes

- Update voting contract to 3.12.0 (id: 0fbab53fa4a462dd648f5f7ba9bcfface4e31d26b14d0b66a48848a103be7324)
  - PAR-6173: New invocation using 'int' instead of 'long' as length
- Update BPO contract to 3.21.0 (id: dd00f3a62e4eff029b83d798e57a8da6c46ec0db171681e7a208c75b8dbca7ef)
  - PAR-6265: New invocation using 'int' instead of 'long' as length
- Update large oracle contract to 3.21.0 (id: d829bba317b7253e1717906b06c94b0536f9f66d33a87e476d369c75fba21938)
  - PAR-6157: Only burn stakes in dispute if amount is greater than zero 
  - PAR-6012: Use longValueExact from Unsigned256
- Update rewards contract to 3.15.0 (id: 1adf0c451e96d6a8c92bed78a858f92313f92faedb3d7a6584d38a687525544f)
  - PAR-6170: New invocation with int as list size
- Update preprocess contract to 3.20.0 (id: 96a037a99611f62acd0b373b881f5789a0d142a193467d5762db49e2cf65830e)
  - PAR-4813: New invocation that deduct preprocessing fees from requesting transaction
- Update public deploy contract to 3.2.0 (id: e2ca4826eb0339ceb79cab09bcb0b72320d3035c695e25403860daca6c0e3c8e)
  - Update pub wasm binder to 3.14.0
    - PAR-5306: removed support for sending from original sender
- Update real wasm deploy contract to 3.12.0 (id: 1903fb717de1fe2154d2216f0c6b0349adb28e24f7f2767e30cb770f6a294408)
  - Update real binder to 3.21.0
    - PAR-6015: Input mask batches are now correctly requested
    - PAR-6031: Keep track of requested input mask batches
    - PAR-5471: Call contract by forwarding event when nodes send information
    - PAR-5959: Support REAL variables with more than 127 bits

## v3.10.0

#### Changes

- PAR-6797: Rosetta should be able to handle transfers with a memo
- PAR-6834: Prepare adresses for Ethereum PARTI BYOC
- PAR-6833: Create a REST resource for accessing Synaps contract

#### Registered votes

- Update BPO contract to 3.22.0 (id: 03c5d6ef660d19685b08c71f068ab9d635b9b9c49c5adf8224fa048a4cc6c08a)
  - PAR-6779: Only check on BP identity when resigning as BP
- Update mpc token to 3.19.0 (id: 1f3c9b22d82283305f1889e69d67aa0ba3d2f0bdacd4dac8680142df6ff62513)
  - PAR-6796: Streamline usage of state#isLocked
- Update account plugin to 3.34.0 (id: 4bb0fddec04e8135160fe56f26dee139789b9fbfc41155100863aefedd27171e)
  - PAR-6649: BYOC transfer invocations using Unsigned256
- Update synaps contract to 3.16.0 (id: 0261a0da3c785c57673cd5c6a53f4d17aa0d8ad506f2df306da074824ac75ac1)
  - PAR-6821: Include city in BP address
  - PAR-6833: KYC flow

## v3.11.0

#### Changes

- PAR-6846: Extend client with ability to get blocktime relative to UTC time
- PAR-6646: Make fast track node use new notify invocation
- PAR-6700: Use new invocation when sending broadcasts to BPO
- PAR-5488: Make byoc-service listener based

#### Registered votes

- Update BPO contract to 3.23.0 (id: 588c651b894602ef00c4886a6f23c3f70feec46cb2fb3e9034f111a581d972aa)
  - PAR-6703: Use new invocation on Voting contract
- Update account plugin to 3.36.0 (id: 58148400d190face196cb06e71ba248bd58ebc4999ec28e71c381287cc41e4b7)
  - PAR-6848: Theoretic overflow possibility in burn logic
  - PAR-5833: Create a proper DISASSOCIATE_TOKENS to handle token disassociation
- Deploy BYOC token contract for ETH at address 014a6d0fd09fe2e6853a76caedcb46646ab7ee69d6 (id: 97cafbe0d840e7cf5c5fc2f93cd7ab1ec53bd6829860c9712de0e27257af3f14)
- Deploy BYOC token contract for BNB at address 0137f4da8ad6a9a5305383953d4b3a9c7859c08bea (id: 82741d1f1e7a489fd85ccf969d2edd075c3b94699462557a7b93a2059bc506da)
- Deploy BYOC token contract for POLYGON_USDC at address 01e0dbf1ce62c4ebd76fa8aa81f3630e0e84001206 (id: 2b426e0b3877028427fe0b1c3ed3d58c04db034b40331ffe24e6f514c68d19ba)

## v3.12.0

#### Changes

- PAR-5623: New java client
- PAR-5470: Extend information sent in sync events
- ZK Node
  - PAR-7444: Attestation handling ignores attestation with id 0
  - PAR-6399: Prepare for next protocol version of REAL

#### Registered votes

- Update BPO contract to 3.31.0 (id: ba7db15ed1923d52f718e86d0dc4e181abc5685c6a3e0d2d4dd2c750693235c9)
  - PAR-7263: Improve code quality in tests
  - PAR-6133: Allow triggering a new committee when the current committee is 1 month old
- Update mpc token to 3.28.0 (id: 1977958b4031846119949977048645c7a6abc2e6290100616ff4eb04f1d87c13)
  - PAR-5833: Proper naming for disassociating tokens from contract
  - PAR-6873: All two-phase-commit should have RETRY changed to ABORT only
  - PAR-7267: Improve code quality of unittests
- Update BYOC token contract to 3.4.0 (ETH id: 52a62561a5a92751dc420b58983b0f288d923d028929e97159966d0168b07591, BNB id: ddfa8fccf13b353e028c3368195f660ccf76dcb311b552ef0ab2cef27ad80af9, POLYGON_USDC id: f7c423484112cf9fbff177206ea54b2bd7bff02b74443e2ee331e33632c252d4)
  - PAR-7517: Avoid removing all allowances when decreasing to zero
  - PAR-7518: Re-add allowance if a transfers_from is aborted

## v3.13.0

#### Changes

- PAR-7248: Avoid pom dependencies between node and contracts
- PAR-4244: Stability for unittests
- PAR-5588: Support for compressed event packets in flooding network

#### Registered votes

- Update BPO contract to 3.34.0 (id: dbf6815cadbf8a81a77cd8461b632e6eb33513f39cd4fbd6433bec154aef1107)
  - PAR-7472: Handle failing association of tokens 
  - PAR-7512: Remove producer stuck in WAITING_FOR_CALLBACK
  - PAR-7299: Streamline creation of contract RPC
- Update mpc token to 3.38.0 (id: c9d3192142f351bda77ba213cb4d2fa6348fdb45cb56302ae5a66bafc5045dfc)
  - PAR-7299: Streamline creation of contract RPC
  - PAR-7648: Remove dependency on Mockito
  - PAR-6650: Use Unsigned256 for transfer of BYOC
- Update account plugin to 3.59.0 (id: 54aa14ac8c8012f4797cc20e60208fcb90b060607eddef0b14328f9f121b3c5b)
  - PAR-6028: Code cleanup and additional documentation
  - PAR-7656: Account for gas when rounding coins to pay for transactions
- Update rewards contract to 3.31.0 (id: 1aae099944b193974fdcedcb529164a92d3e44056e5cace348aaf3503accda4d)
  - PAR-7299: Streamline creation of contract RPC
  - PAR-7648: Remove dependency on Mockito
  - PAR-7802: Calculate threshold for producers count not exactly equal to 3t+1

## v3.14.0

#### Changes

- PAR-821: Remove counting of mutated bytes in serialization library
- ZK node:
  - PAR-7561: Decrease number of threads used for fetching preprocessed material
  - PAR-7738: Improved handling of versioned computations
- BYOC service:
  - PAR-5928: Fix potential nullpointer in peer discovery

#### Registered votes

- Update large oracle contract to 3.38.0 (id: f6732caac710be8897bb26c47e2944b145b9dccde957efc5fe9c29402eaf425d)
  - PAR-7480: Code quality improvements in tests
  - PAR-7299: Streamline creation of contract RPC
  - PAR-7648: Remove dependency on Mockito
  - PAR-7247: Use new ASSOCIATE/DISASSOCIATE invocations
- Update bpo contract to 3.44.0 (id: f4d2d49c4ec3ba252f94775fc63016e9f8a5cdb1c6047c857543e67305c85a13)
  - PAR-7247: Use new ASSOCIATE/DISASSOCIATE invocations
  - PAR-6701: Remove old invocation for ADD_BROADCAST_BITS
- Update voting contract to 3.31.0 (id: 62818ff2b39fb7650e8c57817a105ee88cc05c0413e5fa27ca8cf56bab6b6485)
  - PAR-6702: Remove old invocation for RECEIVE_COMMITTEE_INFORMATION
  - PAR-7299: Streamline creation of contract RPC
  - PAR-7683: Improve codequality in tests
- Update preprocess contract to 3.49.0 (id: 085855831f3a4cb66b15692bf5e76b5ed651a79708b153f0025af330d5413346)
  - PAR-6769: New invocation with updated fee calculation
- Update zk node registry contract to 3.25.0 (id: e370501d63c33bf12d0ac565fd706a7959a9f80b66f285e6e8588807eb2b4383)
  - PAR-6590: Invocation to pay staking fees
  - PAR-7247: Use new ASSOCIATE/DISASSOCIATE invocations
- Update real wasm deploy contract to 3.25.0 (id: 28c73dd135a6aacea67ab960d5f7bc8b0ff023cb6859f9a6764cd3337d06bcc0)
  -  PAR-7406: Add supported client and binder versions
  - Update real binder to 3.50.0
    - PAR-5557: Batching for bitwise MPC
    - PAR-5985: New NEXT_VARIABLE_ID instruction
    - PAR-6584 Empty invocation tops up gas
    - PAR-6769: Use new preprocess invocation
    - PAR-6399: Restructure for versioning of binder
    - PAR-7400: New OUTPUT instruction
    - PAR-7444 Fixed attestation id
    - PAR-7434: Added XOR Support
- Update pub wasm deploy contract to 3.31.0 (id: c8386fa355e7915fae08b3555624eaf28a515672fa59f2942fbe104809864f02)
  - PAR-7648: Remove dependency on Mockito
  - PAR-7406: Add supported client version
  - Update pub wasm binder to 3.41.0

## v3.15.0

#### Changes

- PAR-1197: Abort shutdown if it was triggered due to a temporary problem
- PAR-6398: Replay containing large sync events
- PAR-7671: Allow getting system contract states in abi format
- PAR-8224: Remove usage of json state mapper from core blockchain

#### Registered votes

- Update bpo contract to 3.51.0 (id: 84aeab28fbf8ebc7b0dec0e7d15374e0211f3713141e00f2f1ecef68062ca279)
  - PAR-8344: Active participants should not be set to zero when migrating before active participants have been selected

## v3.16.0

#### Changes

- PAR-8465: Rolling logs for rocks db
- PAR-7866: Partially compressed block in block response
- PAR-3218: Updated dispute handling for retired oracles

#### Registered votes

- Update BYOC token contract to 3.19.0 (ETH id: f5f3474b2678b48e1fa6a798968e632d8c0801ff9e951c60a4ad9da7eb2aa6ad, POLYGON_USDC id: 931bce489720e5c89161032195254336c01e1930fb3d3c178170748c5d0add78, BNB id: 0fe310283841f177720b412a4a6e219ab133138f1c74f5f95d022929743e41c9)
  - PAR-8302: Allow spender to call ABORT
  - PAR-8356: Allocate explicit gas for callbacks
- Update mpc token to 3.45.0 (id: 4bacbf23b69b6f1348dd1a74137219d69dd849c954e1679ccc989fdd84cdc659)
  - PAR-8356: Allocate explicit gas for callbacks
- Update real wasm deploy contract to 3.32.0 (id: 7e0f28c7328f84296589f98e67ead05e88c210b68d1381556778422acb3fc8db)
  - Update real binder to 3.65.0
    - PAR-6590: Add invocation to extend deadline
    - PAR-5857: Support user created events in ZK contracts

## v3.16.1

#### Registered votes

- Update account plugin to 3.59.1 (id: 81c890aa17c9a53b21f071106ebc2a0e73bf9392d659d59c9e0ba1f97605032d)
  - Hotfix for proper cleanup of pending unstakes

## v3.17.0

#### Changes

- PAR-8512: Blockchain should expose state of block when it is finalized
- PAR-6299: Allow node to listen for price oracles

#### Registered votes

- Update account plugin to 3.81.0 (id: 165b33c5d93dfe1772c861f479a034fb95f212d5233a590b74b0632fb95eddab)
  - PAR-6794: Remove old unused invocations
  - PAR-6032: Additional input validation when changing coin rates

## v3.18.0

#### Changes

- PAR-8739: Event catch up should terminate early
- PAR-8731: Support memo in Rosetta API

## v3.19.0

#### Changes

- PAR-8791: Avoid event validation for already known events
- PAR 8790: Include memo in response from Rosetta construction/parse

## v3.20.0

#### Changes

- PAR-8741: Validate network identifer in Rosetta getBlock
- PAR-8794: Additional metadata for operations and transactions in Rosetta

#### Registered votes

- Update account plugin to 3.88.0 (id: 176ee029053dbe28eaa51cf7bcfc74cce4e2fcb98fa307efbfe15edf8751e33f)
  - PAR-8736: Bug when handling conversion rate with large numerator
- Update mpc token to 3.59.0 (id: 9c45d209c1cf209e013ab76e1e331d9cb95d641cc230ba8f82ba572e473bd0d4)
  - PAR-8455: Rewrite contract with abi-generation
- Update synaps to 3.50.0 (id: f4c5b71da7d775ffa80ccdc92afd4a63cb4f0f70b0ca78a125e8ca4da1a325aa)
  - PAR-7299: Streamline creation of contract RPC
  - PAR-8461: Rewrite contract with abi-generation
- Update large oracle to 3.59.0 (id: 22dc94806739bc411b9d4965cfc8c7151254c3ab2712ce7afdcb41e99d693c7a)
  - PAR-8454: Rewrite contract with abi-generation
- Update voting to 3.51.0 (id: fe79f053051c82c4b37d386699a7994d1e085ae53b5e45c5f842fc1f9789fc64)
  - PAR-8458: Rewrite contract with abi-generation
- Update bpo to 3.66.0 (id: baf1dc976b07626e549988718f3c3421905a712b0e483968f04395def12a65b7)
  - PAR-8453: Rewrite contract with abi-generation
- Update foundation to 3.49.0 (SALE id: 560bfac981d40d2ef67ac4cc3b0a49231b1d423d94b6a2df58ead91f1d4fcc31, ECOSYSTEM id: 8f85f2d5ce13f04d5ae13dc23dfe3dcc81001e8471d9c3d7470632dd3decfff8, TEAM id: ee10219c83172e0e9083a6079f50c581eb91475c987543ad5e6ff5040455b6ec, RESERVE id: db77c909e77531f67574acfc9d733a6a6803606ccb2312f9a1a1cfdc653a0c48)
  - PAR-7299: Streamline creation of contract RPC
  - PAR-7655: Improve codequality in tests
  - PAR-8509: Rewrite contract with abi-generation
  - PAR-8775: It should be possible for the council to burn tokens in the pools

## v3.21.0

#### Changes

- PAR-8830: Introduce randomised delay for transactions to fee distribution

## v3.22.0

#### Registered votes

- Update rewards to 3.51.0 (id: 83d5896c0308882a6931cae5b1607947d7e0ee50cc6eaca02fa1a21a10046f2b)
  - PAR-8356: Allocate explicit gas for callbacks
- Update pub deploy contract to 3.31.0 (id: f844cc5737b8a45975dae2cea968601155d885d2da5836b5627fce1de143c824)
  - Update pub binder to 3.59.0
    - PAR-6590: Add invocation to extend deadline
    - PAR-5857: Support user created events in ZK contracts
- Update real wasm deploy contract to 3.32.0 (id: 0a6043716be6550253c7510e489a2143f40f549b4f9c294cf374431bc6599566)
  - Update real binder to 3.72.0
    - PAR-6590: Add invocation to extend deadline
    - PAR-5857: Support user created events in ZK contracts

## v3.23.0

#### Changes

- PAR-7676: TCP client should handle the fact that binder and contract can change by contract upgrade

#### Registered votes

- Update voting to 3.57.0 (id: 78e475df81a6e13d793a12dfd43df3979b50e3f3a9cb09c976fdde6c1738a345)
  - PAR-8726: Determine poll id from content instead of letting user supply it
- Update bpo to 3.73.0 (id: 0418f9dd1feb55440fa2cda962da9eb4b1cfc06a88f82705ce76fe05a17cb2be)
  - PAR-8540: Continuously register and deregister should not trigger a new committee

## v3.24.0

#### Changes

- PAR-8403: New protocol version in real-node allowing multiple pending openings
- PAR-7868: Extend web client with ability to have return value on PUT

#### Registered votes

- Deploy price oracle for ETH/USD on Ethereum (id: 5fd162fc5ae41581bab71bf9fa8812f3004602df015b15e87a77b39c065acc61)

## v3.25.0

#### Changes

- PAR-7869: Expose transactions in pending blocks from ledger
- PAR-8954: Price oracle should only consider finalized blocks when handling disputes
- PAR-8946: Prepare address for price oracle for BNB/USD

#### Registered votes

- Deploy price oracle for BNB/USD on Ethereum (id: 1521c2d3eb012e5d0a11fb4fb8c1a03e69fd1c05dbe79b62abea0ee4feb18872)
- Update voting to 3.67.0 (id: 614a857f38ac4cb466fd516230a93e6bff7533221d55ed00a965e167e584da43)
  - PAR-8964: Preserve polls on contract upgrade
  - PAR-8943: Make it possible to propose a vote that invokes another contract
  - PAR-8458: Improve documentation

## v3.26.0

#### Changes

- PAR-8946: Listen for changes to BNB price oracle

#### Registered votes

- Update account plugin to 3.102.0 (id: 2d0f5fb50404e9111ef98c55a79b761c9329cfcc40a57541983a591c841819af)
  - PAR-8474: Cleanup of migration
  - PAR-9002: Storage fee registered as blockchain usage

## v3.27.0

#### Changes

- PAR-8933: Expose performance information through large oracle status
- PAR-9091: Price oracle should not notify for already published round
- PAR-9053: More granular price oracle error handling

## v3.28.0

#### Changes

- PAR-7868: Implement service for chain API
- PAR-7869: Implement service for shard API
- PAR-9241: Allow specifying a persistent peer in config overrides

#### Registered votes

- Update voting to 3.72.0 (id: 7525406e7614b05974a1a73e11d0a2a2ac01bb2c55faf0dc9ffa5d7ab2b44d57)
  - PAR-9015: Make invocation short name part of the rpc when invoking another contract
- Update account plugin to 3.106.0 (id: e4bfcf5750a8667cbefcf87f15f94162f480e7d086d3d132041888684c2703e3)
  - PAR-8448: Allow ice staking towards public contracts
  - PAR-8449: Ice contracts when they run out of storage fees
- Set gas price for 1000 ice stakes (id: b907375724f752ce3ed23d9e40c9a7256767a50aef6079e085f27cf3a491a237)
- Update mpc token to 3.77.0 (id: 1c50b898684a285f5d6a3cf45e0717f08f3913fad5b91f9f583d2e8f8b7acf8b)
  - PAR-8469: Allow ice staking towards public contracts
- Deploy byoc orchestrator with ETH, POLYGON_USDC, and BNB (id: 703fd628f49b84c6a8e9991bab32a7acaf952da7827c227e5aa26af2a850b0d0)
  - Update byoc incoming to 3.65.0
    - PAR-7299: Streamline creation of contract RPC
    - PAR-8457: Rewrite contract with abi-generation
    - PAR-8474: Cleanup of migration
    - PAR-8457: Improved documentation
  - Update byoc outgoing to 3.65.0
    - PAR-7299: Streamline creation of contract RPC
    - PAR-8456: Rewrite contract with abi-generation
    - PAR-8474: Cleanup of migration
    - PAR-8456: Improved documentation
  - Update byoc token to 3.39.0
    - PAR-8272: Improve documentation
    - PAR-8648: Rewrite contract with abi-generation
  - Update price oracle to 3.77.0
    - PAR-8537: Improved documentation
    - PAR-7300: Handle coins and reference contracts with other number of decimals

## v4.0.0

#### Changes

- PAR-9259: Remove executor from flooding network to ensure ordering

## v4.1.0

#### Registered votes

- Update real wasm deploy contract to 4.4.0 (id: bee6c53d0a6abc8885b14051f0e745ba4a08b992487338a1828d938ae81afb44)
  - PAR-8664: Rewrite contract with abi-generation
  - Update real binder to 4.7.0
    - PAR-9367: Allocate more gas when invoking self
    - PAR-6493: RealWasmBinder allows contract to run a specific function from it's ZK Computation
    - PAR-6497: Added stress tests for many variables and many computations.
    - PAR-8638: Added support for dynamic inserts and extracts in ZK Computation
    - PAR-9247: Avoid emitting zero-shifts in ZK Computation
    - PAR-9190: Added long division example from zk-compiler
    - PAR-9343: Fix crash on shifts in ZK Computation
    - PAR-8695: Meta-circuit now supports integers up to 16383 bits (Two byte LEB).

## v4.2.0

#### Changes

- PAR-8554: Expose memory information in large oracle status

#### Registered votes

- Update real wasm deploy contract to 4.10.0 (id: d510edeabbb48d064e9020511ac9c78e6aca5021efba96c21add3c6dec627e68)
  - PAR-9450: BYOC-token is a system contract instead of governance

## v4.3.0

#### Changes

- PAR-9488: Improved error message REAL node is unable to load variable

#### Registered votes

- Update account plugin to 4.12.0 (id: 28a1eae7c1e679eff4034320dbdcf7b95df052abcd076364f4c02d26d386940b)
  - PAR-8488: Simplify creation of Balance object
  - PAR-9523: Gas allocated to an outbound event does not correspond to work at the time of event creation

## v4.4.0

#### Changes

- PAR-9538: Improved logging in REAL node preprocessing

#### Registered votes

- Update ZK node registry to 4.20.0 (id: 539c9b069d583b73399da3d25bbf1a4d80ecb8fb8950412931ef200e054b7b84)
  - PAR-8666: Rewrite contract with abi-generation
- Update REAL preprocess to 4.20.0 (id: 99621431bb6612d6b8d5128b53d13adf98879bfd56704ce2c2cef66f02a08f36)
  - PAR-8665: Rewrite contract with abi-generation
- Update BYOC incoming to 4.19.0 (id: 16b7c1654fe4a7bbb36e95469732fcf4a8d35b64b435c1d92c63cf80eebd4540)
  - PAR-9155: Allow oracle to request rotation after a month
- Update BYOC outgoing to 4.20.0 (id: 8ef3a920011bc66a8247c3507f9d0d36e97bce904d442635cd430de516a8b536)
  - PAR-9168: Allow oracle to request rotation after a month
  - PAR-9171: Require withdrawal amount less than maximum

## v4.5.0

#### Changes

- PAR-9614: Improve errormessage when putting transaction
- PAR-7335: Granular processes for price oracle handling in byoc-service
- PAR-9576: ZK network close outbound connectors on timeout
- PAR-9538: Additional logging when establishing ZK network
- PAR-9788: Ensure that auto-vote are able to handle INVOKE_CONTRACT

#### Registered votes

- Update pub deploy to 4.28.0 (id: cee3759f2610517fc0e3af603b624cd3119b0c93988a0f98868918b94a7db342)
  - PAR-8649: Rewrite contract with abi-generation
  - PAR-9273: Support different binders simultaneously
- Update BYOC token contract to 4.27.0 (id: b974c88ec53299ce049ca3359f65826ff44b80b613f5157f4151adff451eb47e)
  - PAR-9575: Change ABI to properly use u128

## v4.6.0

#### Changes

- PAR-9850: Fine-grained fee error message
- PAR-9927: Price oracle process uses latest round when nudging an ended challenge period

#### Registered votes

- Update Synaps KYCB contract to 4.29.0 (id: 8781bb0a838e5db0341e46849dece2de8b7803df6ef0bb82ebad52f7d987df5c)
  - PAR-8649: PAR-9732: Only remove KYC registration if BP is successfully registered on BPO
- Update REAL deploy contract to 4.29.0 (id: d392fe6c985be29cc831559f7c91761b7d2229cc875b8bcb2a4b458390dc84e3)
  - PAR-9273: Support multiple binders to improve backwards compatibility 

## v4.7.0

#### Changes

- PAR-9430: CLI for generating node configuration
- PAR-9529: CLI for submitting KYC data

## v4.8.0

#### Changes

- PAR-8592: REAL node web3 client to allow reading logs from EVM
- PAR-10170: Improved usability for node operator CLI
- PAR-10180: Account for remaining gas when signed transactions fails to spawn event. Behind feature flag 'REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION'.

#### Registered votes
- Update Price Oracle contract to 4.44.0 (id: b2774851bed8411ba6f04b770594cad973b0180d0b04ff6dbc5691a030676260)
  - PAR-9133: Remove any price updates from an oracle when it is removed
  - PAR-8956: It is possible to start a dispute instead of publishing a price for an ended challenge period
- Update REAL wasm binder to 4.33.0 (id: 423ae2a0ba4245f42b45c337d75bbb55b750691631821aa8063c62b9fd260e03)
  - PAR-8403: contracts control secret variables
  - PAR-9625: Sign extension implementation in real-wasm-binder
  - PAR-10098: Moved versioning out of wasm-invoker
- Update pub wasm binder to 4.34.0 (id: b77def5ffb0856322fdef64e9ce16aa7cd05979fa719aefd58c3a85adacb54de)
  - PAR-9625: Sign extension implementation in real-wasm-binder
  - PAR-10098: Moved versioning out of wasm-invoker
- Enable feature flag 'REGISTER_GAS_FOR_FAILING_SIGNED_TRANSACTION' (id: c967766a144a5019290223f13f4f15fe7807ef3eb64e270c77b65c16750e51ee)

## v4.9.0

#### Changes

- PAR-9538: More robust handling of preprocessing in real-node

## v4.10.0

#### Changes

- PAR-9756: Prepare client for fetching binary AVL trees from user contracts
- PAR-9622: Make server able to return binary AVL trees from user contracts
- PAR-9557: Real-node: Code cleanup in variable handlers

## v4.11.0

#### Changes

- PAR-9309: Persisted gas accounting in the blockchain executed transactions

## v4.12.0

#### Changes

- PAR-10474: Expose transaction cost in rest interface
- PAR-9564: Introduce plugin for shared object storage
- PAR-9098: Make price oracle retry when getting an error from web3
- PAR-9557: Code quality improvements in real node
- PAR-9522: Use docker-compose in node register script
- PAR-10170: Set port to 8080 in RegisterNodeCli

#### Registered votes

- Update pub wasm binder to 4.44.0 (id: 7ffb3d3c5acd5ef1698c459b5f2f552ea5daf9e17bdb983814e79b2acf8b392a)
  - PAR-9369: Allow contracts to store information in external AvlTree
- Update REAL wasm binder to 4.50.0 (id: 6f504f4db585f4b2c643254c1b59cdfb2cee25e4582ad53ceca4032c87f0c64c)
  - PAR-9369: Allow contracts to store information in external AvlTree
  - PAR-9415: Refactoring of state for preprocessed materials

## v4.13.0

#### Changes

- PAR-10170: Allow base64 encoded keys in node registration CLI
- PAR-9623: Ensure REAL node are able to catch up on previously finished computations
- PAR-10610: Improved error message from StateAccessor
- PAR-10439: Method for getting next entries from AvlTree
- PAR-10642: Received transaction not added to pending

## v4.14.0

#### Changes

- PAR-10469: Ensure chain endpoint for fetching account supports OPEN_ACCOUNT_CREATION
- PAR-10460: REST API to query AVL trees
- PAR-10479: Client support for AVL tree REST API

#### Registered votes

- Update large oracle contract to 4.71.0 (id: 21dea2a769599fc83937512e4c8c0bd5a1edd74c2bc73bddab218479069806a5)
  - PAR-8474: Cleanup migration code
  - PAR-8454: Improve documentation
  - PAR-9255: Don't release pending tokens when retrying oracle fulfilment
  - PAR-9256: Allow clearing of pending oracle stakes after 1 month
- Update voting to 4.71.0 (id: 901b5c4355b4f6cde71045085691126a13f9b972bcc11292d5956d9d2a13b7cc)
  - PAR-9572: Allow updating shared object storage through a vote
- Update ZK node registry to 4.71.0 (id: 7007c845186149bdffd758bd3740c79da5c904f96a4f7feecdb498005df116b3)
  - PAR-8369: Allow nodes to indicate which EVM chains they are able to read from
  - PAR-9662: Ensure ZK nodes have enough stakes when allocated instead of when registering
- Deploy shared object store plugin (id: bc5a0bbc0cb49f9a12e04f84f76de55fb270cd0093fd13a88c733686d7c0ef2c)
  - PAR-9567: Initial version of shared object storage plugin
- Update REAL deploy to 4.69.0 (id: 21969f352fb8fe084622e397dacca5d0bb17b4881eab08c30e708712bb363047)
  - PAR-8376: Allow users to select nodes based on EVM capabilities
  - PAR-9570: Save new binders in shared-object-storage
- Update pub deploy to 4.71.0 (id: c4846eb3706ec2671526c4c7232af34e474c1ce3a319d4fc16c54c8e6c8576f3)
  - PAR-9569: Save new binders in shared-object-storage

## v4.15.0

#### Changes

- PAR-10850: Bug fix for proof of possession in register CLI

## v4.16.0

#### Changes

- PAR-10911: Only fetch EVM logs for a small block range per query
- PAR-10887: Support direct json deserialization of AvlTree
- PAR-8784: Initial support for EVM subscriptions in real-node

#### Registered votes

- Update zk preprocess to 4.83.0 (id: 9809853e74cb6221b28de89ecc9d7fab45cef4ce7e411870eddb1959cd00375a)
  - PAR-10367: Support for binders to request a shortname when material are done
- Update pub wasm binder to 4.56.0 (id: 2c44b9db15d76fe5fb11a73ec77a06b8357d67ba889908b3498fd0fea8be0131)
  - PAR-10795: Adjusted gas cost for AVL operations
  - PAR-10596: Support for iteration and length of AVL trees
- Update REAL wasm binder to 4.60.0 (id: f6f399a11328e7c07c4505626b6f4c7cbe30a35c54904fab699886c99be292d0)
  - PAR-9627: Parallel instruction dispatch in ZK circuit
  - PAR-10367: Cleanup in preparation for ABI
  - PAR-8784: Initial support for EVM subscriptions
  - PAR-10795: Adjusted gas cost for AVL operations
  - PAR-10596: Support for iteration and length of AVL trees

## v4.17.0

#### Changes

- PAR-10911: Persist found EVM logs, allow deployment time search to continue when rate-limited
- PAR-11135: Additional logging in flooding network
- PAR-9134: Clean up naming and handling of block validity depth
- PAR-11046: Ensure chain API knows about shared object store plugin

## v4.18.0

### Changes

- PAR-10813: Ensure transaction inclusion in block
- PAR-8999: Ensure object roots are serialized last
- PAR-11285: Optimize fasttrack shutdown protocol

#### Registered votes

- Update REAL wasm binder to 4.67.0 (id: 5498c2219373c56455f3fc1e6da912e0fcc4e33468b8fc71732e4bd87c5c141d)
  - PAR-10557: Use AvlTree structure for ZK variables, attestations and EVM events
  - PAR-10367: Use preprocess callback with shortname

## v4.19.0

### Changes

- PAR-11371: Ensure a minimum number of outbound connections in flooding
- PAR-11367: node-register.sh fails to send valid KYC submit
- PAR-11365: Prepend 0x when interacting with chainlink reference contracts
- PAR-11285: Ensure fast-track clear already appended justified blocks

## v4.20.0

### Changes

- PAR-11419 Extended Status Endpoint for Nodes
- PAR-11420 Increase fast track network delay

## v4.21.0

### Changes

- PAR-11424: Log long transactions
- PAR-11445: Extend status endpoint with latest block time for all shards

## v4.22.0

### Changes

- PAR-11478: Add node version to status endpoint

## v4.23.0

### Changes

- PAR-11384: Use TLS identity in connections
- PAR-11546: Fix 'Graceful shutdown of TLS protocol might hang'

## v4.24.0

### Changes

- PAR-11420: Decrease fast track network delay to 15 seconds
- PAR-11554: Change host to public key
- PAR-11595: Node should not connect to itself

## v4.25.0

### Changes

- PAR-11618: Remove synchronized on get stage
- PAR-11312: Improve performance of signing

#### Registered votes

- Update voting contract to 4.105.0 (id: f5f4bc72593d7a793485047a35be296a95b1b5c58cb2c6aa0160ef1e93a90d54)
  - PAR-11633: Allow vote to remove a contract
- Remove and re-deploy byoc outgoing for USDT (id: 85e907c3f18eb2f585b4bf7499e5afefda7cdd973d76df72eebf7d6aba807f15)


## v4.26.0

### Changes

- PAR-11638: ZK-network improved configuration validation

#### Registered votes

- Update fast track plugin to 4.66.0 (id: bc29c63ed19e7fd42a12de9b699bbada38bec079933e7ec508931b440c5a5cee)
  - PAR-11825: Don't throw exception on unknown committee

## v4.27.0

### Changes

- PAR-11961: Verify signature and addresses when constructing in Rosetta
- PAR-9140: Better error handling in PriceOracleProcess

## v4.28.0

### Changes

- PAR-11461: Smaller interface for using transaction client
- PAR-12054: Avoid closing connections when receiving future events

#### Registered votes

- Update mpc-token to 4.127.0 (id: 24e9216d438c62bcccc2e91bfaf4de2653c055fda62eca7d50366883f880caea)
  - PAR-10396: Invocations for appointing a custodian
  - PAR-12122: Fix error messages in DISASSOCIATE_ICE
  - PAR-12124: It should be possible to "force abort" a pending ice stake
  - PAR-12232: Introduce invocation for system update to set locked state
  - PAR-10315: Introduce invocations callable by custodian
- Update REAL wasm binder to 4.87.0 (id: 0c14dad00e584224b8e1eeb2b9a6f918d3841d034e29be6be4ce1ce8682ec0c0)
  - PAR-12274: Fix: Binder sometimes request too many triples
  - PAR-11908: Fix: Nodes does not send enough gas for large variables
- Update account plugin to 4.80.0 (id: 340c73fcb384ea8c196fb351164ca6bc7015609463daaa747a3348d568e71886)
  - PAR-9270: Improved unit tests for Signed256
  - PAR-9305: Use mutable version of Balance to improve readability
  - PAR-9101: Add checksum field for tokens in custody
  - PAR-10337: Introduce invocations callable by custodian
  - PAR-11793: Also account for delegated stakes when counting stake amount

## v4.28.1

#### Registered votes

- Update account-plugin to 4.80.1 (id: e03694c344452e4c580551410a425b2b7b913533970831c1f14598a79c2c9230)
  - PAR-10396: Hotfix for handling BYOC transfers with amounts exceeding Long.MAX_VALUE correctly

## v4.29.0

### Changes

- PAR-12486: Fix return type in client getLatestBlockBeforeTime 
- PAR-11578: Custodian enabled transfers are reflected as Rosetta operations

#### Registered votes

- Update mpc-token to 4.134.0 (id: 9c8c2b3107cf1fb06f90af0fdc9184cd67c7bb885945ad608195b64a54c42ad3)
  - PAR-12475: Avoid sending a two-phase commit/abort if the transaction has already been aborted
- Update voting to 4.129.0 (id: 437e1374304b5fc79759799f1e51ce6cec434bb26c4a98cc9430bec89208d981)
  - PAR-12514: Introduce INVOKE_LOCAL_ACCOUNT_PLUGIN update type
- Abort/commit stuck transfers (id: 2f24620b6c58566fd4da31621939bda237c871701265d1cdf655d0f6d35faef9)
- Unlock token transfers (id: 8ddf81cb741ea0cc3b9e8bc612ee31f06b9418ccd5e735c0087c7e0714eb1080)

## v4.29.1

#### Registered votes

- Composite poll 0e8e4137d8e0b505135798f7ad446c31e6897e169d74384a77afd44db05bd099
  - Update large-oracle to 4.136.0
    - PAR-8772: Introduce REQUEST_SIGNATURE for governance update
  - Request signatures for BYOC patch

## v4.30.0

### Changes

- PAR-9063: Account info endpoint in client
- PAR-8903: Rosetta: Implement the search endpoint

## v4.31.0

### Changes

- PAR-12768: Increase EVM oracle read interval

#### Registered votes

- Update bp orchestration contract to 4.136.0 (id: c73e47175321fc3417ff0f6740fac230aaf0f341ae9bec3825626cba00153bfe)
  - PAR-12770: Allow governance contracts to trigger a new committee when it has been active for 1 day
- Update large oracle contract to 4.140.0 (id: c62e86c56791355833004ba44006a9692dadee419960795018835c8b9534050b)
  - PAR-12770: Add invocation to ask for a new committee when out of signatures
  - Release tokens after 14 days instead of 28
- Update mpc-token to 4.145.0 (id: 588680e61810bdcbb9a4d481310c2ecbf122928d88f4be696b2f96e180133c94)
  - Add invocation for checking vested tokens for other

## v4.31.1

#### Registered votes

- Update byoc token contract to 4.136.0 (id: 1f2bf0117631f763a9ac8c5c8f7d2d8c89605699b9c03440d4f5c10d9bcb0dc9)
  - PAR-11561: BYOC transfers on BYOC token contract should fail after appointing a custodian
- Update account plugin to 4.86.0 (id: 76a1589cfbac4df99e13c697a735b99b2450c3dbbf225c7c9f1762a23eab728c)
  - PAR-12936: Adjust pause release end date to 2024-03-20
  - PAR-12533: Deduct coin balance should be disabled for accounts with an accepted custodian

## v4.32.0

### Changes

- PAR-12864: Method overloads in client for getting state at specific block time
- PAR-12944: Fix query params non deterministic order

## v4.33.0

### Changes

- PAR-12940: node-register tool should work for both docker compose v1 and v2
- PAR-12946: Fix node-register tool for KYB

## v4.34.0

### Changes

- PAR-13218: Pretty print JSON config generated by the CLI config generator
- PAR-12804: Rosetta: /call method for account gas

## v4.35.0

### Changes

- PAR-12283: Thread names should include shard identifier for logging

## v4.36.0

### Changes

- PAR-12283: Logs should include shard
- PAR-12943: Node register tool can validate KYC requests

#### Registered votes

- Update 'BYOC Token Contract' to 4.154.0 (id: 6df37b726552e6761b0e4cbdacea9ee9975f58529dce8847a6e8274a191ab95c)
  - PAR-12518: Assert transfer is not to the same account
- Update 'BYOC Incoming Contract' to 4.154.0 (id: af182db4e6a7adce95718dd2bb7764428cb64b37552a5dabc05f14178f785ad5)
  - PAR-12949: Allow governance votes to request a new small oracle
- Update 'BYOC Outgoing Contract' to 4.158.0 (id: ed29e7e2258f769730d59d2fe9417745b5b50c8f4edf6132237d5b8bda58f50b)
  - PAR-12949: Allow governance votes to request a new small oracle
- Update 'Large Oracle Contract' to 4.162.0 (id: a89bdcfbb35033e06265ca4f19b9a01bb0c162be0a896c744f8ebdeb57d3b018)
  - PAR-12572: UNLOCK_OLD_PENDING_TOKENS should check if requests should be fulfilled

## v4.37.0

### Changes

- PAR-13182: BLS performance improvements

#### Registered votes

- Update 'BYOC Token Contract' to 4.162.0 (id: 1ea8a6e08b81aea9da9c38a8a90ca6d2678355af637866175abe3703a3e2af3a)
  - PAR-13608: Bug-fix: Contracts unable to transfer BYOC

## v4.38.0

### Changes

- PAR-12411: Prepare interfaces for upgradeable contracts

#### Registered votes

- Update 'REAL deploy Contract' to 4.167.0 (id: 0966c4e70c198e23a9b3560fa97319f7395e152026890ed589b4c239b3a22b1d)
  - PAR-13087: Remove deploy with evm support
- Update 'ZK node registry' to 4.166.0 (id: 7a03585a21ec28f64a84cd3dfb8ed0b168a46718da0d0e157135de009eb5656e)
  - PAR-10358: Allow governance vote to disable a ZK node
  - PAR-13087: Remove evm chain support information
  - PAR-10919: Add scores to ZK nodes
- Update 'REAL wasm binder' to 4.104.0 (id: 0832174b62233c954a71775787193ab811bc7602b90fc222a751d7dc9b3e8ac6)
  - PAR-10920: Use new notify on zk-registry with computation status
  - PAR-9902: Support for shortnames in onComputeComplete and onVariableInputted

## v4.39.0

### Changes

- PAR-12985: Handle contract upgrade event in contract-test

## v4.40.0

### Changes

- PAR-13088: Refactor EVM log handling in real-node

#### Registered votes

- Update 'ZK Node Registry Contract' to 4.173.0 (id: c8ad02862f65c36e7cca52962738d64e47482609e467f7d2074bafbf3c9e86a7)
  - PAR-14080: Consistent handling of trailing slashes

## v4.41.0

### Changes

- PAR-13430: REAL node require off chain inputs to have a pending input
- PAR-8370: Improved error messages regarding EVM in REAL node
- PAR-14154: REAL node trims the rest endpoints for whitespaces and slashes
- PAR-5807: Initial version of OpenAPI generated server interfaces

#### Registered votes

- Update 'BP Orchestration Contract' to 4.179.0 (id: e5d085f36660c136b0a28ca7e2ad962a86f02879d1d205ad001531ee2d62fd77)
  - PAR-8453: Improved documentation
  - PAR-11561: Rename account plugin invocation DISASSOCIATE_INVOCATION_BYTE to DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_INVOCATION_BYTE
  - PAR-14037: Fix: When registering in BPO with too few stakes the producer can get stuck in 'waiting for callback'

## v4.42.0

### Changes

- PAR-13232: Avoid mix up of KYC and KYB in node register tool
- PAR-14307: Align isAlive timeout
- PAR-14276: Stabilize tests
- PAR-11777: Improve block identifier validation in Rosetta API

## v4.43.0

### Changes

- PAR-14325: Client ensureInclusionInBlock does not properly retry on missing transaction
- PAR-14166: node-register.sh: Make transaction cost message clearer
- PAR-14279: Improve OpenAPI spec

## v4.44.0

### Changes

- PAR-14281: Extend OpenAPI spec with AVL endpoints
- PAR-12762: Introduce UpgradeContractTransaction
- PAR-10798: Compute changed keys between two AVL trees
- PAR-13088: REAL node supports binder with EVM heartbeat
- PAR-3692: Fast-track node are unable to read block state storage on boot
- PAR-14402: Switch OpenAPI contract state endpoint to serve binary states

## v4.45.0

### Changes

- PAR-10884: REST exposes modified AvlTree keys between two block times
- PAR-14631: Ensure that ExecutedTransaction#read gracefully handles when the event storage is incomplete

#### Registered votes

- Update 'Large Oracle Contract' to 4.217.0 (id: 34712632e53b4a625234127d0b1a6f5aeb116ccf892d4696c7e1cda26946b95d)
  - PAR-9639: Allow users to reserve tokens to not be used for new work
- Update 'REAL WASM Binder' to 4.124.0 (id: aea5764bbb4dbe803821bfb69a3b7eba57733d27b3acc0a07a2766af96e3676c)
  - PAR-14092: Improve error message for ZK computation
  - PAR-14481: Place on-compute-complete shortname before binder arguments when invoking self
  - PAR-13088: Nodes sends heartbeat for EVM subscriptions 

## v4.46.0

### Changes

- PAR-14437: Extend status endpoint with BYOC information

## v4.46.1

### Changes

- Increase fast-track network delay

## v4.46.2

### Changes

- PAR-15018: Introduce cache for expected producer computation

## v4.46.3

### Changes

- PAR-15018: Cache synchronized

## v4.46.4

### Changes

- Revert back to normal fast-track network delay

## v5.0.0

### Changes

- PAR-12535: Simpler and stricter Address
- PAR-14855: AvlTree should not allow null keys
- PAR-14979: Endpoint exposing size of avl tree
- PAR-13091: Allow fetching metrics for block interval

## v5.1.0

### Changes

- PAR-13903: Allow running transaction in the block when starting initialising the blockchain
- PAR-14378: Price oracle service should not notify if opt'ed out

## v5.2.0

### Changes

- PAR-15571: Better logs and retries when fetching pre-process material
- PAR-11338: Avoid Flooding fast-track Metrics When Catching Up
- PAR-12171: Increase performance of fast track metrics
- PAR-15635: Rosetta should know of mpc-token-mpc20 contract
- PAR-15778: Do not default to empty operations when unable to locate transaction

#### Registered votes

- Update 'REAL WASM Binder' to 5.9.0 (id: b659f88e1584e03c8588b075ee8fe1c0202f33b0c896fa695fe979bae6567604)
  - PAR-15569: Fixed a NPE in the ping contract functionality used to signal that ZK functionality is no longer needed.
  - PAR-15012: Subtraction of 64+ bit Sbi now produces the correct results.
  - PAR-15012: Improved performance of Sbi negation and subtraction
  - PAR-14315: Support unsigned less-than for Sbi
  - PAR-14786: Support negate for Sbi of sizes other than 32
  - PAR-15421: Enforce ZK branch limit on function calls

## v5.3.0

#### Registered votes

- Deploy 'MPC Token MPC20 Contract' (id: e3be7d789e07b10896db3ec849f376141da06d2ea0eb3eafbdb6ae34a03a7da1)
- Update 'BYOC MPC20 Contract' to 5.38.0 (id: 26d9b0e1b5f55e3c13015a67aa3238cd4fe838c9a425409e262d45583463620a)
  - PAR-15155: Abort overwrites allowance
- Update 'Account Plugin' to 5.13.0 (id: d7da85ab9faba15b04fa85147cacd811d979fce061131e286118d1c6c7e5339c)
  - PAR-12938: Remove old migrations after deployed
  - PAR-12600: Improve errormessage for unknown invocation
  - PAR-13943: Better error message when deducting more coins than available
  - PAR-11561: Remove old unused invocations
  - PAR-15159: Remove TGE realignment since all relevant vestings have been updated on-chain already
  - PAR-15061: Allow contracts to hold MPC
  - PAR-15427: Tokens on deleted contracts should be moved to context free

## v5.4.0

### Changes

- PAR-13380: Extend node-register.sh with report-active command that allows sending the transaction to mark an inactive producer as ready to participate again
- PAR-8463: Change node config to have a map of API urls instead of fixed keys

#### Registered votes

- Update 'Block Producer Orchestration Contract' to 5.46.0 (id: 31168b9ed82bf69077f2405bef7ef9417942350608ace11f0710834a08e8a006)
  - PAR-13380: A producer not participating in selection of new oracle are marked as inactive requiring the producer to send a transaction before being able to be included in a future committee 

## v5.5.0

### Changes

- PAR-12151: Avoid sending duplicate DEPOSIT transactions

## v5.6.0

### Changes

- PAR-16991: Fix node-register.sh report-active
- PAR-16253: Improve test of EVM datasource

#### Registered votes

- Update 'ZK Node Registry Contract' to 5.57.0 (id: 558c3a2c67d33f33fda0104640713d8ae787b3cedd999c49ccb25a495af65ca1)
  - PAR-9642: Keep track of the amount of free staked tokens instead of the total
  - PAR-9642: Allow ZK node to reserve tokens to not be used for future work

## v5.7.0

### Changes

- PAR-9446: Support price oracles on other external chains than Ethereum

#### Registered votes

- Composite poll (id: d8a23e706cc707c535e05eae5a8230e0d734500876fed30f9c6081905882ac0f)
  - Update byoc orchestrator to 5.61.0
    - PAR-9446: make it possible to read from other supported EVM chains
  - Update price oracle contract to 5.59.0 through byoc orchestrator
    - PAR-9446: make it possible to read from other supported EVM chains
    - PAR-14222: Allow a price oracle to opt-out of sending prices to make it easier to deregister with a running node
    - Note: Before a node registers on a price oracle from a chain other than Ethereum it is necessary that the node has been updated to this release

## v5.8.0

#### Registered votes

- Update 'BP Orchestration Contract' to 5.67.0 (id: a2c35c45b4d24b88b40064d08a717d09bd7bd4fa0c48bebc0c9d14017532f8a9)
  - PAR-15325: Track version of producers and allow specifying minimum required version
  - PAR-16969: Trigger new committee transaction should fail if not yet possible

## v5.9.0

### Changes

- PAR-17167: Rosetta API returns failed operations for failing transactions
- PAR-17266: Improved code quality and documentation for ChainStateCache

#### Registered votes

- Update 'Account Plugin' to 5.31.0 (id: 27c363af181abc5e347ddd168c98e7d8a1a9ede5dde713e762544ec8679ac514)
  - PAR-16264: Improve documentation of states
  - PAR-16907: Extend contract invocations to allow adding and removing MPC and BYOC
- Add WMPC as a new BYOC (id: 235474dfce3a72fda8fd715c90a5f9a17717c62ec106ac47b8d105e54b96ecf8)
- Deploy 'MPC Wrap Contract' (id: e02ff1c72e9f7b711e10b1fe546f13b3d3ec5599d4272885dc408043c51d7807)
  
## v5.10.0

### Changes

- PAR-15794: Improve code quality of Rosetta TransactionParser code

## v5.11.0

### Changes

- PAR-17064: Improve test quality in blockchain

#### Registered votes

- Update 'PUB WASM Binder' to 5.25.0 (id: 569ae7655f0616e23e25ac48fc65b5b84afc57f59d0a12bc60ee014bfdf30be4)
  - PAR-17232: Extend Wasm interpretter to handle updated specification, allowing use of rust 1.82
- Update 'REAL Deploy Contract' to 5.87.0 (id: fc444127e4ae371f3c28f2cd96a5fcda6374b93ae44668f951201711c1af280a)
  - PAR-16649: Specify number of preprocess materials to prefetch for a binder
- Update 'REAL WASM Binder' to 5.34.0 (id: a9385e0bd9d950b574c60fbbce1b02fcac862bc867943a8bf7b98b7a908e69e4)
  - PAR-17232: Extend Wasm interpretter to handle updated specification, allowing use of rust 1.82
  - PAR-17127: Configurable default optimistic mode
  - PAR-16622: Only three node reactions required for input and open variables

## v5.12.0

### Changes

- PAR-17518: Register resource exposing the OpenApi specification on reader nodes
- PAR-8760: Token rewards automation tool

#### Registered votes

- Update 'REAL Preprocess Contract' to 5.92.0 (id: 9695487dc71edb66a4a7ebafeeaa1d313d2b98182aa619099499aa0ef21d50e1)
  - PAR-17329: Make it possible to retry a batch
- Deploy WBTC BYOC (id: c67fc7792a71993855e8478882c705cb2692f574d31b5829c563e093648eaf2b)

## v5.13.0

### Changes

- PAR-17546: More explicit error messages for BinderContract default methods
- PAR-17808: Improve test stability for node config
- PAR-17666: Extend main to discover token bridges
- PAR-17753: Improve test stability in real-node

## v5.14.0

### Changes

- PAR-18008: Fix problem with sending multiple transactions from standalone real node 
- PAR-18024: Test utility SerializationChecker 

#### Registered votes

- Update byoc-orchestrator to 5.105.0 (id: dfbb27d1cf42b53019a501aa94aab8f4c7b69891c200eb5352e06aa36725f2e3)
  - PAR-14787: Remove old ADD_BYOC invocation and callback
  - PAR-17318: Extend BYOC orchestrator with token bridges
- Update price oracle contract to 5.102.0 (id: c815fcc5558f9a62f7617f637372efb28b06fcc98fbc9ed58e0bc42972ffd2a4)
  - PAR-17142: Oracle should not be allowed to overwrite own price

## v5.15.0

### Changes

- PAR-17329: REAL node sends reject batch when failing preprocess
- PAR-18102: Add function to check if executed transaction tree includes any failed transactions
- PAR-18200: Document BinderContext gas accounting more explicitly

## v5.16.0

### Changes

- PAR-15325: Report node version on start

## v5.17.0

#### Registered votes

- Update public-deploy to 5.120.0 (id: f90456ea9434b46fda1e1189895683cffc3ae60048af60b108a5b8db8dd1be87)
  - PAR-14053: Add invocation to upgrade existing contracts
- Update 'PUB WASM Binder' to 5.35.0 (id: e3016fb2d33a1091c8d455fca55fdce580680bc6a351140f210be9a95d63e377)
  - PAR-14554: Implement hooks for upgrading contracts

## v5.18.0

### Changes

- PAR-16699: Make full REAL computation able to run with only three nodes

#### Registered votes

- Update zk-node-registry to 5.129.0 (id: ac4de7ddf6a76f235a8b301c7217ae0a29d8d718702c79bd6cf5cbb2664cef9c)
  - PAR-18680: Support for requesting specific ZK nodes
- Update real-deploy to 5.130.0 (id: a014141a3202be46b9d692c67ad1c7102a5984bd667e1a8ae82dde6e714b33f6)
  - PAR-18680: Support for requesting specific ZK nodes
- Update real-preprocess to 5.132.0 (id: 6a9e310718705b046b98b2ec6702341762dfa60afa31df975abb6530740e8d20)
  - PAR-18795: Add action for manually retrying a preprocessing batch

## v5.19.0

### Changes

- PAR-17444: Make it possible for REAL node to recover lost shares from the other nodes

#### Registered votes

- Update BP orchestration to 5.144.0 (id: bf47cbd3a0208bf720166e1bc0f08d225ec00d608073bd4725edbf78505fac69)
  - PAR-18199: Add invocation that returns list of active BPs
- Update large-oracle to 5.140.0 (id: 232ae362c663d0994fc8b1b2c57af18a358624746854d15c5d1d74c56a3112ae)
  - PPAR-18503: Introduce invocation for checking if nodes are still part of the large oracle
- Update byoc-incoming to 5.141.0 (id: 363e80d0d2a701d5780207a4053e0d894b6921c2ac75828f245e8ce7c6f8a027)
  - PAR-18190: Improved javadoc
  - PAR-18503: Allow rotating oracle if it contains nodes who are no longer BPs
- Update byoc-outgoing to 5.138.0 (id: 12255824992e15a4b59e664fce3bbfcf40c68f897ba64a9c7c2fc0152a0a043d)
  - PAR-18503: Allow rotating oracle if it contains nodes who are no longer BPs

## v5.20.0

### Changes

- PAR-19145: Recovery resource path for triples in real-node
- PAR-19226: Easier construction of BlockchainTransactionClient

## v5.21.0

### Changes

- PAR-19388: Increase default receive timeout to 5 min

#### Registered votes

- Update foundation to 5.157.0 (SALE id: e2a17364cda3c0c786540ea3a6560fc4723928dd93ce2ddcd79ea1f377a4c42a, ECOSYSTEM id: 5e1c2e832bbedacf7511ebe0c5f45973cefcfea8aa6eefacaf6cda80ffa844f5, TEAM id: aef1f5d17dcea82ed2847f332c7136e50778b6ceeb55f87b3e746707e9f606b1, RESERVE id: 59a6988d22d6b1208b402559d78b07208d3f05b538299a0b0ab56f68105e4c9b)
  - PAR-8509: Improve documentation
  - PAR-18259: Improve tests
  - PAR-18359: Ensure no transfer amount is zero or negative
  - PAR-17577: Expand with support for allowances
  - PAR-18797: Introduced action to change council

## v5.22.0

### Changes

- PAR-18802: Method to skip elements when getting entries from an AvlTree
- PAR-19225: Client should retry if unable to put transaction when ensuring inclusion
- PAR-15997: Rosetta implementation to handle delegations with timestamps
- PAR-19226: Use client based on chain and shard api
